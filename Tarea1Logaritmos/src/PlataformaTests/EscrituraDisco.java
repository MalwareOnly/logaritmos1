package PlataformaTests;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class EscrituraDisco {
	
	/*nota: esta funcion no sobreescribe el archivo, sino que escribe desde la ultima linea*/
	public static void save(String filename, String string){		
		PrintWriter printWriter = null;
		try {
            printWriter = new PrintWriter(new FileWriter(filename,true));
            printWriter.println(string);
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
            unsupportedEncodingException.printStackTrace();
        } catch (IOException e) {
			e.printStackTrace();
		} finally {
            printWriter.close();
        }
	}

}

