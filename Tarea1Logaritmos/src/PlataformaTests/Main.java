package PlataformaTests;

import java.util.ArrayList;
import java.util.Arrays;

import AlgoritmosOrdenamiento.QuickSort;

public class Main {
	
    
    public static void main(String[] args) {
    	RunTest tests = new RunTest(1000);
    	Runnable qsRunnable = tests.getQuickSortRunnable();
    	Runnable msRunnable = tests.getMergeSortRunnable();
    	Runnable bsRunnable = tests.getBubbleSortRunnable();
    	Runnable isRunnable = tests.getInsertionSortRunnable();
    	Thread qsThread = new Thread(qsRunnable);
    	Thread msThread = new Thread(msRunnable);
    	Thread bsThread = new Thread(bsRunnable);
    	Thread isThread = new Thread(isRunnable);
    	qsThread.start();
    	msThread.start();
    	bsThread.start();
    	isThread.start();
    }
 

}
