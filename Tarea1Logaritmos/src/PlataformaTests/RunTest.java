package PlataformaTests;

import java.util.ArrayList;

import AlgoritmosOrdenamiento.BubbleSort;
import AlgoritmosOrdenamiento.InsertionSort;
import AlgoritmosOrdenamiento.MergeSort;
import AlgoritmosOrdenamiento.QuickSort;
import AlgoritmosOrdenamiento.Sort;
import GeneradoresArreglos.RandomArray;

public class RunTest {
	Sort algoritmo;
	ArrayList<Integer> arreglo;
	String filename;
	int numberOfTimes;
	
	public RunTest(Sort algoritmo,String filename){
		this.algoritmo=algoritmo;
		this.filename=filename;
	}

	public RunTest(int numberOfTimes) {
		this.numberOfTimes=numberOfTimes;
	}

	public Sort runOnce(int arraySize){
		arreglo=RandomArray.randomGenerator(arraySize);
		algoritmo.setExecutionTime(0);
		algoritmo.setNumberOfComparisons(0);
		algoritmo.sort(arreglo);
		return algoritmo;
	}
	
	public void runAndSave(int arraySize){
		EscrituraDisco.save(filename,this.runOnce(arraySize).toString());
	}
	
	public void runMultipleTimes(int arraySize, int numberOfTimes){
		for(int i=1; i<=numberOfTimes;i++){
			runAndSave(arraySize);
		}
	}
	
	public Runnable getQuickSortRunnable(){
 	   return new Runnable(){
		     public void run(){
		    	System.out.println("empezo qs");
	    		double x=System.currentTimeMillis();
		        RunTest test= new RunTest(new QuickSort(),"qs.txt");
				for(int j=0;j<=10;j++){
				   	test.runMultipleTimes((int)Math.pow(2, 10+j), 10000);
				}
		     }
		   };
	}
	
	public Runnable getMergeSortRunnable(){
	 	   return new Runnable(){
			     public void run(){
			    	System.out.println("empezo ms");
		    		double x=System.currentTimeMillis();
			        RunTest test= new RunTest(new MergeSort(),"ms.txt");
				    for(int j=0;j<=10;j++){
				        test.runMultipleTimes((int)Math.pow(2, 10+j), 10000);
				    }
			     }
			   };
	}
	public Runnable getBubbleSortRunnable(){
	 	   return new Runnable(){
			     public void run(){
			    	System.out.println("empezo bs");
		    		double x=System.currentTimeMillis();
			        RunTest test= new RunTest(new BubbleSort(),"bs.txt");
				    for(int j=0;j<=10;j++){
				       	test.runMultipleTimes((int)Math.pow(2, 10+j), 10000);
				    }
			     }
			   };
	}
	public Runnable getInsertionSortRunnable(){
	 	   return new Runnable(){
			     public void run(){
			    	System.out.println("empezo is");
		    		double x=System.currentTimeMillis();
			        RunTest test= new RunTest(new InsertionSort(),"is.txt");
				    for(int j=0;j<=10;j++){
				       	test.runMultipleTimes((int)Math.pow(2, 10+j), 10000);
				    }
			     }
			   };
	}
}
