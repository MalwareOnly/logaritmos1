/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AlgoritmosOrdenamiento;

import java.util.ArrayList;

/**
 *
 * @author magonzal
 */
public abstract class Sort extends ProfileableAlgorithm{
    ArrayList<Integer> A;
	public boolean sorting=true;
	double p=0.9;
    public Sort(){};
    @Deprecated
    public Sort(ArrayList<Integer> A){
        this.A=A;
    }
    @Deprecated
    public abstract ArrayList<Integer> sort();
    public abstract ArrayList<Integer> sort(ArrayList<Integer> arreglo);
    public ArrayList<Integer> getArray(){return A;}
}
