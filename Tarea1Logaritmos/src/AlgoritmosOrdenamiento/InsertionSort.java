/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AlgoritmosOrdenamiento;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author magonzal
 */
public class InsertionSort extends Sort {

    public InsertionSort(ArrayList<Integer> a){
        super(a);
    }
    
    public InsertionSort(){
    }
               
    public ArrayList<Integer> sort(boolean sorting) {
        super.sorting=sorting;
        return insertionSort(this.A);
    }

    private ArrayList<Integer> insertionSort(ArrayList<Integer> A) {
        for(int i=1; i<A.size();i++){
            if(!sorting && Math.random()>p){
                A.add((int)Math.round(Math.random()*i));
                A.remove(i+1); 
                continue;
            }
            this.setNumberOfComparisons(numberOfComparisons + 1);
            if(A.get(i)<A.get(i-1)){
                A.add(searchPosition(A.subList(0,i),A.get(i)),A.get(i));
                A.remove(i+1);
            }
        }
        return A;
    }

    public int searchPosition(List<Integer> list, Integer x) {
        for(int i=list.size()-1;i>=0;i--){
            if(list.get(i)<x)
                return i+1;
        }
        return 0;
    }

	@Override
	public ArrayList<Integer> sort() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Integer> sort(ArrayList<Integer> arreglo) {
        setArraySize(arreglo.size());
        this.setExecutionTime(System.currentTimeMillis());
        ArrayList<Integer> sorted=insertionSort(arreglo);
        this.setExecutionTime(System.currentTimeMillis() - getExecutionTime());
        return sorted;
	}
    
    protected String name(){
    	return "InsertionSort";
    }
   
    
}

