/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AlgoritmosOrdenamiento;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author magonzal
 */
public class QuickSort extends Sort{
    
    public QuickSort(ArrayList<Integer> A){
        super(A);
    }
  
    public QuickSort() {
		super();
	}

	@Deprecated
    @Override
    public ArrayList<Integer> sort() {
        ArrayList<Integer> a=this.getArray();
        this.setExecutionTime(System.currentTimeMillis());
        ArrayList<Integer> sorted=quickSort(a);
        this.setExecutionTime(System.currentTimeMillis() - getExecutionTime());
        return sorted;
    }

    private ArrayList<Integer> quickSort(ArrayList<Integer> a) {
        int length=a.size();
        if(length<=1)
            return a;
        int pivot=a.get(length-1);
        ArrayList<Integer> l=new ArrayList<Integer>();
        ArrayList<Integer> g=new ArrayList<Integer>();
        
        for(int i=0; i<length; i++){
            if(a.get(i)>pivot){
            	this.setNumberOfComparisons(numberOfComparisons + 1);
                g.add(a.get(i));
            }
            else if(a.get(i)<pivot){
            	this.setNumberOfComparisons(numberOfComparisons + 1);
                l.add(a.get(i));
            }
        }
        l= quickSort(l);
        l.add(pivot);
        l.addAll(quickSort(g));
        return l;
    }
    
    protected String name(){
    	return "QuickSort";
    }
    
    public ArrayList<Integer> sort(ArrayList<Integer> arreglo) {
        setArraySize(arreglo.size());
        this.setExecutionTime(System.currentTimeMillis());
        ArrayList<Integer> sorted=quickSort(arreglo);
        this.setExecutionTime(System.currentTimeMillis() - getExecutionTime());
        return sorted;
    }
    
    public static void main(String[] args) {
        QuickSort qs=new QuickSort(new ArrayList<Integer>(Arrays.asList(1,4,5,2,8,6,7,3, 10, 13, 100, 50, 38, 45, 33, 55)));
        System.out.println(qs.sort());
        System.out.println(qs.toString());
    }
 
}
