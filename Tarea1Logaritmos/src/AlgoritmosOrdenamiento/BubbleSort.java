package AlgoritmosOrdenamiento;

import java.util.ArrayList;

public class BubbleSort extends Sort{
    public BubbleSort (ArrayList<Integer> a){
        super(a);
    }
    
    public BubbleSort(){
    	
    }

    @Deprecated
    @Override
    public ArrayList<Integer> sort() {
        return bubbleSort(this.A);
    }

    private ArrayList<Integer> bubbleSort(ArrayList<Integer> A) {
        int length=A.size();
        int aux;
        for(int i=0; i<length; i++){
            for(int j=0; j<length-1; j++){
            	this.setNumberOfComparisons(numberOfComparisons + 1);
                if(A.get(j)>A.get(j+1)){
                    aux=A.get(j);
                    A.set(j, A.get(j+1));
                    A.set(j+1, aux);
                }        
            }
        }
        return A;
    }


	@Override
	public ArrayList<Integer> sort(ArrayList<Integer> arreglo) {
		setArraySize(arreglo.size());
		this.setExecutionTime(System.currentTimeMillis());
        ArrayList<Integer> result= bubbleSort(arreglo);
        this.setExecutionTime(System.currentTimeMillis() - this.getExecutionTime());
		return result;
	}
	
    protected String name(){
    	return "BubbleSort";
    }

}