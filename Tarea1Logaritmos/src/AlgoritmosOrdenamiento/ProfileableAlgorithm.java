package AlgoritmosOrdenamiento;

public abstract class ProfileableAlgorithm {
	
	//metrics
	long numberOfComparisons;
	int numberOfSwaps;
	int arraySize;
	double executionTime;
	
	public ProfileableAlgorithm(){
		numberOfComparisons=0;
		numberOfSwaps=0;
		executionTime=0;
		arraySize=0;
	}
	
	protected String name(){
		return "Unnamed";
	}
	
	public String toString(){
		return this.name()
		+";"+getNumberOfComparisons()
		+";"+getExecutionTime()
		+";"+getArraySize();
	}
		
	private int getArraySize() {
		return this.arraySize;
	}
	protected void setArraySize(int size){
		arraySize=size;
	}

	public long getNumberOfComparisons() {
		return numberOfComparisons;
	}

	public void setNumberOfComparisons(long l) {
		this.numberOfComparisons = l;
	}

	public double getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(double d) {
		this.executionTime = d;
	}	
	
}
