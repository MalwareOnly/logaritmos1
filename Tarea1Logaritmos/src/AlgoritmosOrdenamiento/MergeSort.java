/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AlgoritmosOrdenamiento;

import java.util.ArrayList;

/**
 *
 * @author magonzal
 */
public class MergeSort extends Sort{
	public MergeSort(){
		
	}
	
    public MergeSort(ArrayList<Integer> a){
        super(a);
    }
    @Deprecated
    @Override
    public ArrayList<Integer> sort() {
    	this.setExecutionTime(System.nanoTime());
        ArrayList<Integer> result= mergeSort(this.A);
        this.setExecutionTime(System.nanoTime() - this.getExecutionTime());
        return result;
    }

    private ArrayList<Integer> mergeSort(ArrayList<Integer> A) {
        int length=A.size();
        if(length<=1)
            return A;
        int i=0;
        int j=(int)length/2;
        ArrayList<Integer> l=new ArrayList<Integer>(A.subList(i,j));
        ArrayList<Integer> g=new ArrayList<Integer>(A.subList(j,length));
        l=mergeSort(l);
        g=mergeSort(g);
        return merge(l,g);
        //throw new UnsupportedOperationException("Not yet implemented");
    }
    private ArrayList<Integer> merge(ArrayList<Integer> a1, ArrayList<Integer> a2){
        int j=0,k=0;
        int length1=a1.size(), length2=a2.size();
        ArrayList<Integer> A=new ArrayList<Integer>();
        
        for(j=0; j<length1; j++){
            if(k==length2){
                A.addAll(a1.subList(j,length1));
                break;
            }
            this.setNumberOfComparisons(numberOfComparisons + 1);
            if(a1.get(j)<a2.get(k)){
                A.add(a1.get(j));
                continue;
            }
            else{
                A.add(a2.get(k));
                k++;
            }
        }
        A.addAll(a2.subList(k,length2));
        return A;
    }
    
    protected String name(){
    	return "MergeSort";
    }

	@Override
	public ArrayList<Integer> sort(ArrayList<Integer> arreglo) {
		setArraySize(arreglo.size());
    	this.setExecutionTime(System.currentTimeMillis());
        ArrayList<Integer> result= mergeSort(arreglo);
        this.setExecutionTime(System.currentTimeMillis() - this.getExecutionTime());
        return result;
	}
}
