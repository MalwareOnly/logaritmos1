package GeneradoresArreglos;

import java.util.ArrayList;

public class RandomArray {
	
    public static ArrayList<Integer> randomGenerator(int n){
        ArrayList<Integer> a=new ArrayList<Integer>(n);
        for(int i=0; i<n; i++)
            a.add(i);
        for(int i=0; i<n; i++){
            int aux=a.get(i);
            int rndm=(int)(Math.round(Math.random() * (n-1-i))) + i; 
            a.set(i, a.get(rndm));
            a.set(rndm,aux);
        }
        return a;
    }
	
}
